import { createHash } from 'crypto';

// Define the mock functions first
const sendMessageMock = jest.fn();
const promiseMock = jest.fn();

// Mocking the SQS class and its methods
jest.mock('aws-sdk', () => {
    return {
        SQS: jest.fn(() => ({
            sendMessage: sendMessageMock.mockReturnThis(),
            promise: promiseMock
        }))
    };
});

// Use require instead of import for the module you're testing
const { handler, isValidQuantity, isValidSkuId, isValidToken } = require('./handler');

const AWS = require('aws-sdk');
const mockedSQS = new AWS.SQS();

// Helper function to generate a token
function generateToken(userId: string): string {
    return createHash('sha256').update(userId).digest('hex');
}
describe('PlaceOrder Function', () => {
    beforeEach(() => {
        // Set up mock implementations for SQS functions
        sendMessageMock.mockReturnValueOnce({
            promise: promiseMock
        });
        promiseMock.mockResolvedValueOnce({});
    });

    it('should respond with 200 for a valid order', async () => {
        const mockUserId = '123';
        const mockEvent = {
            body: JSON.stringify({ SkuId: 'sku123', Quantity: 5, UserId: mockUserId }),
            headers: { token: generateToken(mockUserId) },
        };

        const response = await handler(mockEvent as any);
        expect(response.statusCode).toBe(200);
    });
});

describe('PlaceOrder Helper Functions', () => {
    it('should return true for valid Sku', () => {
        const testSkuId = "sku123";
        expect(isValidSkuId(testSkuId)).toBe(true);
    });
    it('should return false for invalid Sku', () => {
        const testSkuId = "sku999";
        expect(isValidSkuId(testSkuId)).toBe(false);
    })
    it('should return true for valid Quantity', () => {
        const testQuantity = 15;
        expect(isValidQuantity(testQuantity)).toBe(true);
    });
    it('should return false for Negative Quantity', () => {
        const testQuantity = -1;
        expect(isValidQuantity(testQuantity)).toBe(false);
    })
    it('should return false for Large Quantities', () => {
        const testQuantity = 101;
        expect(isValidQuantity(testQuantity)).toBe(false);
    });
    it('should generate and match correct token for a given UserId', () => {
        const testUserId = '123';
        const testUserIdToken = createHash('sha256').update(testUserId).digest('hex');
        expect(isValidToken(testUserId,testUserIdToken)).toBe(true);
    });
})
