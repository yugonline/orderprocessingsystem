import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { createHash } from 'crypto';
import {SQS} from 'aws-sdk';

const validSkuIds = ["sku123","sku456","sku789"]; //In DB skus that are valid
const sqs = new SQS();
const QUEUE_URL = process.env.QUEUE_URL!;


async function sendToQueue(order: any): Promise<void> {
    const params = {
        QueueUrl: QUEUE_URL,
        MessageBody: JSON.stringify(order)
    };

    try {
        await sqs.sendMessage(params).promise();
    } catch (error) {
        console.error('Error sending message to SQS:', error);
        throw error;
    }
}
export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    //Extract order details from the event body
    const { SkuId, Quantity, UserId } = JSON.parse(event.body || '{}');
    const token = event.headers['token'] || '';  // Assuming the header key is 'token'

    if (!isValidSkuId(SkuId)) {
        console.error(`Order Item Id was invalid: ${SkuId}`);
        return {
            statusCode: 400,
            body: JSON.stringify({ message: 'Invalid SkuId' }),
        };
    }

    if (!isValidQuantity(Quantity)) {
        console.error(`Quantity was invalid: ${Quantity}`);
        return {
            statusCode: 400,
            body: JSON.stringify({ message: 'Invalid Quantity' }),
        };
    }

    if (!isValidToken(UserId, token)) {
        console.error(`UserId was invalid: ${UserId}`);
        return {
            statusCode: 401,
            body: JSON.stringify({ message: 'Invalid Token' }),
        };
    }
    if (isValidToken(UserId, token) && isValidSkuId(SkuId) && isValidQuantity(Quantity)) {
        const orderToQueue = {
            SkuId,
            Quantity,
            UserId,
            status: "STARTED"
        };

        console.log(`Sending order to SQS for processing: ${JSON.stringify(orderToQueue)}`);

        await sendToQueue(orderToQueue);

        console.log("Sent to SQS!");

        return {
            statusCode: 200,
            body: JSON.stringify({ message: 'Order placed successfully' }),
        };
    }

    console.error("This code should never run!! DANGER!!");

    return {
        statusCode: 400,
        body: JSON.stringify({ message: 'Invalid request' }),
    };

}

export function isValidSkuId(skuId: string): boolean {
    return validSkuIds.includes(skuId);
}

export function isValidQuantity(quantity: number): boolean {
    return quantity > 0 && quantity <= 99;
}

export function generateToken(userId: string): string {
    return createHash('sha256').update(userId).digest('hex');
}

export function isValidToken(userId: string, token: string): boolean {
    return generateToken(userId) === token;
}


