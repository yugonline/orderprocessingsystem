import { SQSEvent } from 'aws-lambda';
import {PrismaClient} from "@prisma/client";

const prisma = new PrismaClient();

export const handler = async (event: SQSEvent): Promise<void> => {
    // Each record corresponds to an order in the SQS queue
    for (const record of event.Records){
        const order = JSON.parse(record.body);
        console.log("Adding order to DB");
        //Update order in Database
        const orderResult = await prisma.order.create({
            data: {
                skuId: order.skuId,
                quantity: order.quantity,
                userId: order.userId,
                status: "PLACED"
            },
        });
        console.log('Order Processed: ', order);
    }
};
