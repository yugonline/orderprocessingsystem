import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    const orderId = event.pathParameters?.orderId;

    if (!orderId) {
        return {
            statusCode: 400,
            body: JSON.stringify({ message: 'Order ID is required' }),
        };
    }

    const order = await prisma.order.findUnique({
        where: { id: parseInt(orderId) },
    });

    if (!order) {
        return {
            statusCode: 404,
            body: JSON.stringify({ message: 'Order not found' }),
        };
    }

    return {
        statusCode: 200,
        body: JSON.stringify({ status: order.status }),
    };
};